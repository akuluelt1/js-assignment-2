/* Variables */
var sCounter;
var mCounter;
var dspNumber ="";
var runOnce = 1;
var timer;

function loadButtons(){

    var el = "";
    var buttons = document.getElementById('buttons');

    for(i=1; i<=9; i++){
        el += "<button type='button' name='button' id='"+ i +"' value='"+ i +"' onclick='popTimer("+ i +")'>"+ i +"</button>";
        if(i % 3 == 0){
            el += "<br/>";
        }
    buttons.innerHTML = el;
    buttons.innerHTML += "<button type='button' name='button' id='0' value='0' onclick='popTimer(0)'>0</button>";
    }
}



function popTimer(n){

    if(document.getElementById('timer').innerHTML == "DONE"){
        clearTimer();
    }

    timer = document.getElementById('timer');
    timer.innerHTML += n;
    var output = '';
    for(var i = 0; i < timer.length; i++) {
        output += str.charAt(i);
        if(i % 2 == 1 && i > 0) {
            output += ':';
        }
    }
}


function keyPress() {

    var sound = document.getElementById("audio");
    sound.play();
}


function startTimer() {

    dspNumber = document.getElementById('timer').innerHTML;

    // Set the sCounter and mCounter variables based off of the number typed in by the user
    if (runOnce==1){
        runOnce = 0;
        if (dspNumber<100){
             sCounter = dspNumber;
             mCounter=0;
         }else {
             sCounter = dspNumber %  100
             mCounter = (dspNumber - sCounter) / 100;
         }
     }

     //replace the text content of my div1 below with info
     // about my counter variable in an <h1> tag
     if (sCounter<10) {
         document.getElementById('timer').innerHTML = (mCounter)+ ":0" + (sCounter--);
     } else {
         document.getElementById('timer').innerHTML = (mCounter)+ ":" + (sCounter--);
     }

     document.getElementById("startButton").disabled=true;
     document.getElementById("clearButton").disabled=true;
     // finally, set our timer using the 'setTimeout()' function, and
     // 'recall' this function again in 1000 milliseconds (it will call this method)
     if(mCounter < 0){
         stopTimer();
     }else if(sCounter<0){
         mCounter--;
         sCounter=60;
         timer = setTimeout("startTimer()",1000);
     }else{
         timer = setTimeout("startTimer()",1000);
     }
}


function clearTimer(){
    //call the clearTimeout() function on the window object and make sure
    // to pass in our timer - otherwise it won't know what to clear
    clearTimeout(timer);
    // reset the text in our div1
    document.getElementById('timer').innerHTML = "";

    // enable the start button again
    document.getElementById('startButton').disabled=false;
    // Reset runOnce variable to allow it to run again without refreshing the page
}


function stopTimer() {

    document.getElementById('timer').innerHTML = "DONE";
    $("#timer").fadeOut(750);
    $("#timer").fadeIn(375);
    $("#timer").fadeOut(750);
    $("#timer").fadeIn(375);
    $("#timer").fadeOut(750);
    $("#timer").fadeIn(375);

    var sound = document.getElementById("audio2");
    sound.play();

    // call the clearTimeout() function on the window object and make sure
    // to pass in our timer - otherwise it won't know what to clear
    clearTimeout(timer);

    // enable the start button again
    document.getElementById('startButton').disabled=false;
    document.getElementById('clearButton').disabled=false;
    var element = document.getElementById("glass");
    element.classList.remove("cooking");

    // Reset the global variables
    sCounter = 0;
    mCounter = 0;
    dspNumber = "";
    runOnce = 1;
}
