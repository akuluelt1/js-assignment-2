// declare variables for different uses.

var display = document.getElementsByClassName('display')[0],
    numberValue = document.getElementsByClassName('num'),
    btnAdd = document.getElementsByClassName('add')[0],
    btnSubtract = document.getElementsByClassName('subtract')[0],
    btnDivide = document.getElementsByClassName('divide')[0],
    btnMultiply = document.getElementsByClassName('multiply')[0],
    clearKey = document.getElementsByClassName('clear')[0],
    btnEquals = document.getElementsByClassName('equals')[0];


var curNumber = 0,
    prevNumber = 0,
    afterOperation = false,
    curOperation = undefined;

// get number value
var numloop = function numloop(i) {
  numberValue[i].onclick = function () {
    changeDisplayVal(numberValue[i].innerHTML);
  };
};

for (var i = 0; i < numberValue.length; i++) {
  numloop(i);
}

clearKey.onclick = function () {
  clearAll();
};

btnAdd.onclick = function () {
  doOperation('add');
};

btnSubtract.onclick = function () {
  doOperation('subtract');
};

btnMultiply.onclick = function () {
  doOperation('multiply');
};

btnDivide.onclick = function () {
  doOperation('divide');
};

btnEquals.onclick = function () {
  evaluate(curOperation);
};


function doOperation(operation) {
  if (!curOperation) {
    prevNumber = curNumber;
    curOperation = operation;
    afterOperation = true;
  } else if (!afterOperation) {
    evaluate(curOperation);
    prevNumber = curNumber;
    curOperation = operation;
    afterOperation = true;
  } else {
    curOperation = operation;
  }
};


function clearAll() {
  curNumber = 0;
  prevNumber = 0;
  curOperation = undefined;
  afterOperation = false;
  display.innerHTML = '0';
};


function changeDisplayVal(numString) {
  if (display.innerHTML === '0' || afterOperation) {
    display.innerHTML = '';
    afterOperation = false;
  }
  // if having more than one decimal point, fix it
  if (numString === '.' && display.innerHTML.indexOf('.') > -1) {
    numString = '';
  }
  if (display.innerHTML.length >= 16) {
    // max length
  } else {
    display.innerHTML += numString;
  };
  // set current number
  curNumber = Number(display.innerHTML);
};

// operate process
function evaluate(operation) {
  if (!afterOperation) {
    switch (operation) {
      case 'add':
        curNumber = prevNumber + curNumber;
        break;
      case 'subtract':
        curNumber = prevNumber - curNumber;
        break;
      case 'multiply':
        curNumber = prevNumber * curNumber;
        break;
      case 'divide':
        curNumber = prevNumber / curNumber;
        break;
    }
    if (curNumber.toString().length >= 16) {
      curNumber = Number(curNumber.toFixed(16));
    }
    display.innerHTML = curNumber;
  }
  afterOperation = true;
  curOperation = undefined;
};
